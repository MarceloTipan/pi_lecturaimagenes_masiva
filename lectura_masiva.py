#Programa que, a partir de un dataSet de imágenes, genera sus análogos en:
#Blanco Negro (Binario), Escala de grises, RGB
# importar librería OpenCV
import cv2 as cv
# Funciones de Sistema Operativo
import os
#Funciones numpy
import numpy as np

initSequence=1 # inicio secuencia
numSequences= 100 # fin secuencia
cont_frame = 0 # contador auxiliar para implementar

#Directorio de lectura
path_RGB = "../DATASETS/17flowers/RGB"

# Directorios de escritura
# Imágenes GRAY
path_GRAY = "./Output-Gray"
# Imágenes BINARIAS (B/N)
path_Binary = "./Output-Binary"
# Canal R (RED)
path_R = "./channel-RED"
# Canal G (GREEN)
path_G = "./channel-GREEN"
# Canal B (BLUE)
path_B = "./channel-BLUE"

totalImages = int(len(os.listdir(path_RGB))) # Contabiliza número de archivos

# Bucle recorre desde el inicio a fin en un rango
cont_frame = 0 # contador auxiliar para implementar
for ns in range(initSequence,numSequences+1):
    cont_frame = cont_frame + 1
    dirimages = path_RGB + '/image_' + str(ns).zfill(4) + '.jpg'
    img = cv.imread(dirimages)

    # Transformar a ESCALA DE GRISES la imagen en color
    #gray=cv.cvtColor(img,cv.COLOR_BGR2GRAY)

    #Transforma a BN
    #(thresh, im_bw) = cv.threshold(gray, 135, 255, cv.THRESH_BINARY)

    #Separa a RGB
    b,g,r = cv.split(img)

    #b = cv.extractChannel(img, 0)
    #g = cv.extractChannel(img, 1)
    #r = cv.extractChannel(img, 2)

    print("image_"+str(cont_frame)+".jpg")

    #cv.imwrite(path_GRAY   + '/grayimage_'   + str(ns).zfill(4) + '.jpg', gray)
    #cv.imwrite(path_Binary + '/binaryimage_' + str(ns).zfill(4) + '.jpg', im_bw)

    cv.imwrite(path_R + '/Rimage_' + str(ns).zfill(4) + '.jpg', r)
    cv.imwrite(path_G + '/Gimage_' + str(ns).zfill(4) + '.jpg', g)
    cv.imwrite(path_B + '/Bimage_' + str(ns).zfill(4) + '.jpg', b)

print(dirimages)
print(cont_frame)
print(totalImages)